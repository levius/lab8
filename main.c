#include "bmp_functions.h"
#include "status_codes.h"
#include "sepia.h"
#include <sys/resource.h>

void invoke_function(void (*function)(struct BMP*, struct BMP*), struct BMP* img_in, struct BMP* img_out, char* description) {
    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    function(img_in, img_out);

    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    long res_time = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
    printf("Time elapsed in seconds for %s filter: %.2f\n", description, res_time/1000000.0);

}


int main()
{
    printf("read\n");
    FILE *f = fopen("image.bmp", "rb");
    struct BMP input;
    printf("%s\n", get_read_status_message(readBMP(f, &input)));
    fclose(f);
    printf("read done\n");
    FILE *out_c = fopen("out_c.bmp", "wb");
    FILE *out_asm = fopen("out_asm.bmp", "wb");
    struct BMP output_c;
    struct BMP output_asm;
    invoke_function(sepia_c, &input, &output_c, "C");
    invoke_function(sepia_sse, &input, &output_asm, "ASM");
    printf("write\n");
    printf("%s\n", get_write_status_message(writeBMP(output_c, out_c)));
    printf("%s\n", get_write_status_message(writeBMP(output_asm, out_asm)));
    fclose(out_c);
    fclose(out_asm);
    printf("write done\n");
    return 0;
}

