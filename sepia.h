#ifndef SEPIA_H
#define SEPIA_H

#include <stdlib.h>
#include "bmp_functions.h"

void sepia_c( struct BMP* img, struct BMP* res ) ;
void sepia_sse(struct BMP*, struct BMP*);

#endif 
